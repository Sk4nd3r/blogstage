import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Route } from "react-router-dom";
import store from "./redux";
import firebase from "firebase";

const config = {
  apiKey: "AIzaSyAlo_XvTIH15825nsdiKvUysv9ZXvSLot0",
  authDomain: "sea-stalk.firebaseapp.com",
  databaseURL: "https://sea-stalk.firebaseio.com",
  projectId: "sea-stalk",
  storageBucket: "sea-stalk.appspot.com",
  messagingSenderId: "794725351840"
};
firebase.initializeApp(config);
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Route path={"/"} component={App} />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
