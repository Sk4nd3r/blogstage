import firebase from "firebase";

export async function getCollectionList(collection) {
  try {
    const collectionObject = await firebase
      .firestore()
      .collection("article")
      .orderBy("date", "desc")
      .get();
    return collectionObject.docs.map(item => ({
      ...item.data(),
      id: item.id
    }));
  } catch (e) {
    throw e;
  }
}

export async function getCollectionTopRated(collectiontopRated) {
  try {
    const collectiontopRatedObject = await firebase
      .firestore()
      .collection("article")
      .orderBy("rating", "desc")
      .limit(5)
      .get();
    return collectiontopRatedObject.docs.map(item => ({
      ...item.data(),
      id: item.id
    }));
  } catch (e) {
    throw e;
  }
}

async function getCommentData(comment, id) {
  const user = await firebase
    .firestore()
    .collection("user")
    .where("id", "==", comment.userId)
    .get();
  return { ...comment, userInfo: user.docs[0].data(), id };
}

export async function getArticleDetails(id) {
  try {
    const articleCollection = firebase
      .firestore()
      .collection("article")
      .doc(id);
    const collectionArticleDetailsObject = await articleCollection.get();
    const article = collectionArticleDetailsObject.data();
    let comments = await articleCollection
      .collection("comment")
      .orderBy("date")
      .get();
    const commentsPromise = Promise.all(
      comments.docs.map(item => getCommentData(item.data(), item.id))
    );

    const comentsArray = await commentsPromise;

    const userInfo = await firebase
      .firestore()
      .collection("user")
      .where("id", "==", article.userId)
      .get();

    return {
      ...article,
      comments: comentsArray,
      userInfo: userInfo.docs[0].data()
    };
  } catch (e) {
    throw e;
  }
}

export async function createPost(payload) {
  try {
    await firebase
      .firestore()
      .collection("article")
      .add(payload);
  } catch (e) {
    throw e;
  }
}

export async function uploadImage(image) {
  try {
    const imageSnapShot = await firebase
      .storage()
      .ref()
      .child(image.name)
      .put(image);
    const url = await imageSnapShot.ref.getDownloadURL();
    return url;
  } catch (e) {
    throw e;
  }
}

export async function updateArticle({ id, ...other }) {
  try {
    await firebase
      .firestore()
      .collection("article")
      .doc(id)
      .update(other);
  } catch (e) {
    throw e;
  }
}

export async function createComment(articleId, values) {
  try {
    await firebase
      .firestore()
      .collection("article")
      .doc(articleId)
      .collection("comment")
      .add(values);
  } catch (e) {
    throw e;
  }
}

export async function userListArticle(userId) {
  try {
    const articles = await firebase
      .firestore()
      .collection("article")
      .where("userId", "==", userId)
      .get();
    return articles.docs.map(item => ({
      ...item.data(),
      id: item.id
    }));
  } catch (e) {
    throw e;
  }
}
