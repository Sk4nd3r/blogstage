import React, { Component } from "react";
import { connect } from "react-redux";
import firebase from "firebase";
import classes from "./Register.module.css";
import TextField from "@material-ui/core/TextField";

import LoadingButton from "../../components/loadingButton/loadingButton";

import { withRouter } from "react-router-dom";

import { registerRequest } from "../../redux/action/registerActions";

class Register extends Component {
  state = {
    registerHover: false,
    firstname: "",
    lastname: "",
    email: "",
    password: "",
    cpassword: "",
    errors: []
  };

  componentDidUpdate(props) {
    if (props.fetching && !this.props.fetching && !this.props.error) {
      this.props.history.push("/");
    }
  }

  registerChecked = async () => {
    const errors = [];
    const { firstname, lastname, password, email } = this.state;
    if (
      this.state.firstname &&
      this.state.lastname &&
      this.state.cpassword &&
      this.state.password &&
      this.state.email
    ) {
      if (this.state.password !== this.state.cpassword) {
        errors.push("The password has to match");
      }
      const regx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      if (!regx.test(this.state.email)) {
        errors.push("Invalid email");
      }
    } else {
      errors.push("You must fill all the required fields");
    }

    this.setState({ errors });

    if (!errors.length) {
      this.props.registerRequest({ firstname, lastname, password, email });
    }
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  //creating account

  render() {
    return (
      <form className={classes.form}>
        <span className={classes.text}>Register</span>
        Welcome to the registration page please enter your informations!
        <TextField
          label="First name"
          className={classes.textField}
          value={this.state.firstname}
          onChange={this.handleChange("firstname")}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: { focused: classes.inputLabel, root: "hello " },
            style: { color: "black" }
          }}
        />
        <TextField
          label="Last name"
          className={classes.textField}
          value={this.state.lastname}
          onChange={this.handleChange("lastname")}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: { focused: classes.inputLabel, root: "hello " },
            style: { color: "black" }
          }}
        />
        <TextField
          id="outlined-email-input"
          label="Email"
          className={classes.textField}
          value={this.state.email}
          onChange={this.handleChange("email")}
          type="email"
          name="email"
          autoComplete="email"
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: { focused: classes.inputLabel, root: "hello" },
            style: { color: "black" }
          }}
        />
        <TextField
          id="outlined-password-input"
          label="password"
          className={classes.textField}
          type="Password"
          autoComplete="current-password"
          margin="normal"
          variant="outlined"
          value={this.state.password}
          onChange={this.handleChange("password")}
          InputLabelProps={{
            classes: { focused: classes.inputLabel, root: "hello" },
            style: { color: "black" }
          }}
        />
        <TextField
          id="outlined-cpassword-input"
          label="Confirm Password"
          className={classes.textField}
          type="password"
          autoComplete="current-password"
          margin="normal"
          variant="outlined"
          value={this.state.cpassword}
          onChange={this.handleChange("cpassword")}
          InputLabelProps={{
            classes: { focused: classes.inputLabel, root: "hello" },
            style: { color: "black" }
          }}
        />
        {this.state.errors.map(error => (
          <p key={error}>{error}</p>
        ))}
        {this.props.error && <p>{this.props.error}</p>}
        <LoadingButton
          onClick={this.registerChecked}
          fetch={this.props.fetching}
        />
      </form>
    );
  }
}
const mapStateToProps = state => {
  return {
    fetching: state.register.fetching,
    error: state.register.error
  };
};

const mapDisaptchToProps = dispatch => {
  return {
    registerRequest: payload => dispatch(registerRequest(payload))
  };
};
export default connect(
  mapStateToProps,
  mapDisaptchToProps
)(withRouter(Register));
