import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import classes from "./categories.module.css";
import PaperCategories from "../PaperCategories/PaperCategories";


class categories extends Component {
  categoriesClick = () => {
    this.props.history.push("/categories");
  };

  render() {
    return (
      <div>
        <Paper>
          <div className={classes.info}>
            <h1>
              All categories presented on{" "}
              <span className={classes.infoCat}> Sea Stalk !!</span>
            </h1>
            <span className={classes.infoFind}>
              {" "}
              Find all articles of your favorite subject !
            </span>
          </div>
        </Paper>
        <PaperCategories />
      </div>
    );
  }
}

export default categories;
