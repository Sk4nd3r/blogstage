import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import cssClasses from "./Postbuilder.module.css";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Paper from "@material-ui/core/Paper";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import { categories } from "../PaperCategories/colorCategories/colorCategories";
import { connect } from "react-redux";
import { createPostRequest } from "../../redux/action/createPost";
import LoadingButton from "../../components/loadingButton/loadingButton";
import classe from "./Postbuilder.module.css";

// function TabContainer({ children, dir }) {
//   return (
//     <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
//       {children}
//     </Typography>
//   );
// }

// TabContainer.propTypes = {
//   children: PropTypes.node.isRequired,
//   dir: PropTypes.string.isRequired
// };

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 500,
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    margin: 5,
    minWidth: 120
  }
});

class Postbuilder extends React.Component {
  state = {
    title: "",
    category: "",
    value: 0,
    tab: "Post",
    image: null,
    description: "",
    errors: [],
    src: ""
  };
  handleChangeSelect = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  onPostClick = e => {
    e.preventDefault();
    this.setState({ tab: "Post" });
  };

  onPictureClick = e => {
    e.preventDefault();
    this.setState({ tab: "Picture" });
  };

  onImageChange = e => {
    const fr = new FileReader();
    fr.addEventListener("load", e => {
      this.setState({ src: e.target.result });
      fr.removeEventListener("load", () => {});
    });
    const src = fr.readAsDataURL(e.target.files[0]);
    this.setState({ image: e.target.files[0] });
  };

  onDescriptionChange = e => {
    this.setState({ description: e.target.value });
  };

  createPost = () => {
    const errors = [];
    const { title, category, description, image } = this.state;

    if (!(title || category)) {
      errors.push("title or category missing");
    } else if (!(description || image)) {
      errors.push("description or image required");
    }

    if (errors.length === 0) {
      this.props.createPost({
        title,
        category,
        description,
        image
      });
    }
    this.setState({ errors });
  };

  render() {
    const { classes, theme } = this.props;
    const fr = new FileReader();
    console.log(
      "image",
      this.state.image && fr.readAsDataURL(this.state.image)
    );

    return (
      <>
        <div className={cssClasses.container}>
          <header className={cssClasses.Header}>Create Post</header>

          <div className={cssClasses.select}>
            <Paper className={cssClasses.SelectPaper}>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="categories">Categories</InputLabel>
                <Select
                  value={this.state.category}
                  onChange={this.handleChangeSelect}
                  inputProps={{
                    name: "category",
                    id: "category"
                  }}
                >
                  {Object.keys(categories).map(category => (
                    <MenuItem value={category}>{category}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Paper>
          </div>
          <div className={cssClasses.pageContainer}>
            <Paper className={cssClasses.paper}>
              <form className={cssClasses.form}>
                <div className={cssClasses.tabContainer}>
                  <button
                    onClick={this.onPostClick}
                    className={classNames(
                      cssClasses.tabButton,
                      this.state.tab === "Post" && cssClasses.tabButtonSelected
                    )}
                  >
                    Post
                  </button>
                  <button
                    onClick={this.onPictureClick}
                    className={classNames(
                      cssClasses.tabButton,
                      this.state.tab === "Picture" &&
                        cssClasses.tabButtonSelected
                    )}
                  >
                    Picture
                  </button>
                </div>
                {this.state.tab === "Post" ? (
                  <div className={cssClasses.fieldsWrapper1}>
                    <div className={cssClasses.Title}>
                      <input
                        className={classNames(
                          cssClasses.textField,
                          cssClasses.titleInput
                        )}
                        placeholder="title"
                        onChange={e => this.setState({ title: e.target.value })}
                        value={this.state.title}
                      />
                    </div>
                    <TextField
                      label="Post"
                      multiline
                      classes={{
                        root: cssClasses.textField,
                        formControl: { root: cssClasses.flex }
                      }}
                      margin="normal"
                      variant="outlined"
                      rows={9}
                      onChange={this.onDescriptionChange}
                      value={this.state.description}
                    />
                  </div>
                ) : (
                  <div className={cssClasses.fieldsWrapper2}>
                    {this.state.src && (
                      <img height={"auto"} width={"60%"} src={this.state.src} />
                    )}
                    <input
                      accept="image/*"
                      className={cssClasses.input}
                      id="outlined-button-file"
                      type="file"
                      onChange={this.onImageChange}
                    />

                    <label
                      className={classNames(
                        this.state.src && cssClasses.buttonPadding
                      )}
                      htmlFor="outlined-button-file"
                    >
                      <Button
                        variant="outlined"
                        component="span"
                        className={classNames(cssClasses.button)}
                      >
                        {this.state.src ? "Change" : "Upload"}
                      </Button>
                    </label>
                  </div>
                )}

                {this.state.errors.map(error => (
                  <p key={error}> {error}</p>
                ))}
              </form>

              <LoadingButton
                classes={{
                  root: classNames(classe.post)
                }}
                title={"Post"}
                width="100%"
                onClick={this.createPost}
              />
            </Paper>
            <div className={cssClasses.rulesContainer}>
              <Paper className={cssClasses.Paper2}>
                <ul className={cssClasses.list}>
                  <li>1- Remember to be human</li>
                  <li>2- Behave like you would in real life</li>
                  <li>3- Look for the original source of content</li>
                  <li>4- Search for duplicates before posting</li>
                  <li>5- Read the community’s rules</li>
                </ul>
              </Paper>
            </div>
          </div>
        </div>
      </>
    );
  }
}

Postbuilder.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

const mapDisaptchToProps = dispatch => ({
  createPost: payload => dispatch(createPostRequest(payload))
});

export default connect(
  null,
  mapDisaptchToProps
)(withStyles(styles, { withTheme: true })(Postbuilder));
