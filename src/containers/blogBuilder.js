import React from "react";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Logo from "../components/Logo/logo";
import { connect } from "react-redux";

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  appBar: {
    height: 55
  },
  iconButton: {
    minHeight: "0 !important",
    justifyContent: "space-between",
    color: "white",
    position: "relative",
    width: "100%"
  }
};

class MenuAppBar extends React.Component {
  state = {
    auth: true,
    anchorEl: null
  };

  handleChange = event => {
    this.setState({ auth: event.target.checked });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  render() {
    const { classes, isUser } = this.props;

    return (
      <div className={classes.root}>
        <div className={"app-bar-placeholder"} />
        <div className={"app-bar-opacity"}/>
        <AppBar className={"app-bar"}>
          <Toolbar
            classes={{ root: classes.iconButton + " toolbar-background" }}
          >
          
            <div className={"app-logo"}>
              <IconButton
                aria-label="Menu"
                classes={{ root: "app-name" }}
                color="inherit"
                onClick={this.props.onLogoClick}
              >
                <Logo />
              </IconButton>
              <IconButton
                aria-label="Menu"
                classes={{ root: "app-name" }}
                color="inherit"
                onClick={this.props.onLogoClick}
              >
                Sea Stalk
              </IconButton>
            </div>
            <div>
              <IconButton
                aria-label="Menu"
                classes={{ root: "app-bar-action" }}
                color="inherit"
                onClick={this.props.onCategoriesClick}
              >
                Categories
              </IconButton>
              <IconButton
                aria-label="Menu"
                classes={{ root: "app-bar-action" }}
                color="inherit"
                onClick={
                  isUser ? this.props.onProfileClick : this.props.onLogin
                }
              >
                {isUser ? "Profile" : "Login"}
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
        
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isUser: !!Object.keys(state.register.user).length
  };
};

export default connect(mapStateToProps)(withStyles(styles)(MenuAppBar));
