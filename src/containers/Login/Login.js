import React, { Component } from "react";
import { connect } from "react-redux";
import Button from "../../components/loadingButton/loadingButton";

import TextField from "@material-ui/core/TextField";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import classes from "./Login.module.css";
import { withRouter } from "react-router-dom";
import { loginRequest, logout } from "../../redux/action/loginAction";
import { emailChange, passwordChange } from "../../redux/action/authActions";

class Login extends Component {
  state = {
    loginHover: false,
    registerHover: false,
    errors: []
  };

  componentDidMount() {
    this.props.logout();
  }

  componentDidUpdate(props) {
    if (props.fetching && !this.props.fetching && !this.props.error) {
      const { search } = this.props.location;
      const query = new URLSearchParams(search);
      let path = "/";
      for (let param of query.entries()) {
        if (param[0] === "path") {
          path = param[1];
        }
      }
      this.props.history.push(path);
    }
  }

  loginHoverIn = () => {
    this.setState({ loginHover: true });
  };
  loginHoverOut = () => {
    this.setState({ loginHover: false });
  };
  registerHoverIn = () => {
    this.setState({ registerHover: true });
  };
  registerHoverOut = () => {
    this.setState({ registerHover: false });
  };
  registerClick = () => {
    this.props.history.push("/register");
  };

  emailChange = e => {
    this.props.emailChange(e.target.value);
  };
  passwordChange = e => {
    this.props.passwordChange(e.target.value);
  };
  loginClick = () => {
    const errors = [];
    const { email } = this.state;
    if (!(this.props.email || this.props.password)) {
      errors.push("empty field");
    } else {
      const regx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      if (!regx.test(this.props.email)) {
        errors.push("Invalid email");
      }
    }

    if (!errors.length) {
      this.props.loginRequest({
        email: this.props.email,
        password: this.props.password
      });
    }
    this.setState({ errors });
  };

  render() {
    return (
      <form className={classes.form}>
        <DialogTitle id="form-dialog-title">
          <text className={classes.text}>Login</text>
        </DialogTitle>
        <DialogContentText>
          Welcome to SeaStalk
          <br />
          Please enter your email and password to login
        </DialogContentText>

        <TextField
          id="outlined-email-input"
          label="Email"
          className={classes.textField}
          type="email"
          name="email"
          autoComplete="email"
          margin="normal"
          variant="outlined"
          value={this.props.email}
          onChange={this.emailChange}
        />
        <TextField
          id="outlined-password-input"
          label="Password"
          className={classes.textField}
          type="password"
          autoComplete="current-password"
          margin="normal"
          variant="outlined"
          value={this.props.password}
          onChange={this.passwordChange}
        />
        {this.state.errors.map(error => (
          <p key={error}>{error}</p>
        ))}
        {this.props.error && <p>{this.props.error}</p>}
        {/* <Button onClick={this.handleClose} color="primary">
            Cancel
          </Button> */}
        <Button
          onClick={this.loginClick}
          title={"Login"}
          className={classes.loginButtonRoot}
          fetch={this.props.fetching}
        />

        <Button
          onClick={this.registerClick}
          className={classes.loginButtonRoot}
        >
          Register
        </Button>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {
    fetching: state.login.fetching,
    error: state.login.error,
    email: state.auth.email,
    password: state.auth.password
  };
};

const mapDisaptchToProps = dispatch => {
  return {
    loginRequest: payload => dispatch(loginRequest(payload)),
    emailChange: email => dispatch(emailChange(email)),
    passwordChange: password => dispatch(passwordChange(password)),
    logout: () => dispatch(logout())
  };
};

export default connect(
  mapStateToProps,
  mapDisaptchToProps
)(withRouter(Login));
