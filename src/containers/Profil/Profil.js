import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import classe from "./Profil.module.css";
import Paper from "@material-ui/core/Paper";
import classNames from "classnames";
import natureImg from "../../assests/images/nature.jpg";
import LoadingButton from "../../components/loadingButton/loadingButton";
import { userListArticleRequest } from "../../redux/action/userListArticleActions";
import { logout } from "../../redux/action/loginAction";
import ArticleItems from "../../components/articleItem/articleItem";
import Avatar from "../../assests/images/avatar.png";
import Modal from "@material-ui/core/Modal/Modal";
import Button from "@material-ui/core/Button/Button";

class Profil extends React.Component {
  state = {
    open: false,
    image: null,
    src: ""
  };

  componentDidMount() {
    this.props.getListArticle(this.props.userId);
  }

  goToCreatePost = () => {
    this.props.history.push("/create-post");
  };

  logout = () => {
    this.props.history.push("/");
    this.props.logout();
  };

  onArticleClick = id => {
    this.props.history.push("/details/" + id);
  };

  openModal = () => {
    this.setState({ open: true });
  };

  onImageChange = e => {
    const fr = new FileReader();
    fr.addEventListener("load", e => {
      this.setState({ src: e.target.result });
      fr.removeEventListener("load", () => {});
    });
    const src = fr.readAsDataURL(e.target.files[0]);
    this.setState({ image: e.target.files[0] });
  };

  render() {
    return (
      <div className={classe.paper}>
        <div className={classe.articles}>
          <div className={classe.listeArticle1}>
            <span className={classe.title}>
              Your Posts <div className={classe.titleBorder} />
            </span>
            {this.props.articles.length ? (
              this.props.articles.map(article => {
                return (
                  <ArticleItems
                    imgUrl={article.imageUrl}
                    category={article.category}
                    title={article.title}
                    description={article.description}
                    key={article.id}
                    date={article.date}
                    onClick={() => this.onArticleClick(article.id)}
                  />
                );
              })
            ) : (
              <div className={classe.empty}>
                hmm... {this.props.userName || this.props.email} hasn't posted
                anything
              </div>
            )}
          </div>

          <div className={classe.listeArticle2}>
            <div className={classe.detailsContainer}>
              <div className={classe.img}>
                <div onClick={this.openModal} className={classe.img_img}>
                  <img src={Avatar} alt="nature" width={80} height={80} />
                </div>
              </div>
              <div className={classe.details}>
                <span>{this.props.userName || this.props.email}</span>
              </div>
            </div>
            <div className={classe.post}>
              <LoadingButton
                classes={{
                  root: classNames(classe.button)
                }}
                onClick={this.goToCreatePost}
                title={"Create Post"}
              />
              <LoadingButton
                classes={{
                  root: classNames(classe.logout)
                }}
                onClick={this.logout}
                title={"Logout"}
              />
            </div>
          </div>
        </div>
        <Modal
          onClose={() => this.setState({ open: true })}
          open={this.state.open}
        >
          <div className={classe.imageModalContainer}>
            <div className={classe.imageModal}>
              <div className={classe.imageContainer}>
                <img src={this.state.src} className={classe.profileImg} />
              </div>

              <input
                accept="image/*"
                className={classe.uploadInput}
                id="outlined-button-file"
                type="file"
                onChange={this.onImageChange}
              />

              <label htmlFor="outlined-button-file">
                <Button
                  variant="outlined"
                  component="span"
                  className={classe.uploadButton}
                >
                  {this.state.src ? "Change" : "Upload"}
                </Button>
              </label>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userId: state.register.user.uid,
    articles: state.userListArticle.articles,
    userName: state.register.user.displayName,
    email: state.register.user.email
  };
}

function mapDisaptchToProps(dispatch) {
  return {
    getListArticle: userId => dispatch(userListArticleRequest(userId)),
    logout: () => dispatch(logout())
  };
}

export default connect(
  mapStateToProps,
  mapDisaptchToProps
)(Profil);
