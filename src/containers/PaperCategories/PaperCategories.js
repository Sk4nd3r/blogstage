import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

import classe from "./PaperCategories.module.css";
import Catego from "./catego/catego";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    background: "#fff"
  }
});

function PaperSheet(props) {
  const { classes } = props;
 
  return (
    <div className={classe.articles}>
      <div className={classe.listeArticle}>
        <Paper className={classes.root} elevation={1}>
            <Catego />

        </Paper>
      
      </div>
    </div>
  );
}

PaperSheet.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PaperSheet);
