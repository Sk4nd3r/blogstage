import React from "react";
import {
  categoriesArray,
  categoriesTypes
} from "../colorCategories/colorCategories";

import classes from "./catego.module.css";
const ArticleItem = props => {
  return (
    <div className={classes.articleItem}>
      <div className={classes.categoriesTitles}>
        {categoriesArray.map(category => {
          return (
            <div className={classes.categoryContainer}>
              <div className={classes.txt} style={{ color: category.color }}>
                {category.title}:
              </div>
              <div className={classes.list}>
                {categoriesTypes[category.title].map(type => {
                  return <div className={classes.text}>{type}</div>;
                })}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default ArticleItem;
