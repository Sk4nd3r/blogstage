

const categories = {
  Culture: "#673ab7",
  Loisirs: "#ff5722",
  politics: "#03a9f4",
  Technologies: "#4caf50",
  Divers: "#e040fb"
};

const categoriesTypes = {
  Culture: ["Art", "Cinema", "History", "Religion", "Society"],
  Loisirs: [
    "Games",
    "Personal Diary",
    "Houses & Decoration",
    "Fashion & Beauty",
    "Music",
    "Photos",
    "Sport",
    "Trip"
  ],
  politics: [
    "Business and Economy", 
    "policy"
  ],
  Technologies: [
    "Internet",
    "Computing",
    "Coding",
    "Hardware",
    "Other Technologies"
  ],
  Divers: [
    "News",
    "Associations",
    "Family",
    "General",
    "Humor",
    "Health",
    "Magazine"
  ]
};

const categoriesArray = Object.keys(categories).map(key => {
  return { title: key, color: categories[key] };
});

export { categories, categoriesArray, categoriesTypes };
