import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Paper from "@material-ui/core/Paper";
import firebase from "firebase";
import listeArticle from "../../redux/action/listeArticleAction";
import classe from "./details.module.css";
import ArticleItems from "../../components/articleItem/articleItem";
import articleItemTopRated from "../../components/articleItemTopRated/articleItemTopRated";
import {
  articleDetailsRequest,
  updateArticle
} from "../../redux/action/articleDetailsAction";
import { createComment } from "../../redux/action/commentActions";
import { topRatedRequest } from "../../redux/action/topRatedAction";
import TopRated from "../../components/topRated/topRated";
import Loading from "../../components/Loading/Loading";
import ArticleItemDetails from "../../components/articleItemDetails/articleItemDetails";
import { categories } from "../PaperCategories/colorCategories/colorCategories";
import moment from "moment";
import NatureImg from "../../assests/images/nature.jpg";
import TextField from "@material-ui/core/TextField";
import LoadingButton from "../../components/loadingButton/loadingButton";
import classNames from "classnames";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    background: "#fff"
  }
});

class details extends React.Component {
  state = {
    comment: ""
  };
  async componentDidMount() {
    const id = this.props.match.params.id;
    this.props.articleDetailsRequest(id);
  }

  componentDidUpdate(props) {
    if (
      !this.props.commentFetching &&
      props.commentFetching &&
      !this.props.commentError
    ) {
      this.setState({ comment: "" });
    }
  }

  onCommentChange = e => {
    this.setState({ comment: e.target.value });
  };

  createComment = () => {
    this.props.createComment(this.props.match.params.id, {
      content: this.state.comment,
      date: moment().unix(),
      userId: this.props.user.uid
    });
  };

  updateVote = value => () => {
    const id = this.props.match.params.id;
    this.props.updateArticle({
      id,
      ratingDetails: {
        ...this.props.articleDetails.ratingDetails,
        [this.props.user.uid]: value
      }
    });
  };

  render() {
    const { classes } = this.props;
    const upDisable =
      Object.keys(this.props.user).length === 0 ||
      this.props.articleDetails.ratingDetails[this.props.user.uid] === 1;
    const downDisabled =
      Object.keys(this.props.user).length === 0 ||
      this.props.articleDetails.ratingDetails[this.props.user.uid] === -1;
    return (
      <div className={classe.articles}>
        <div className={classe.listeArticle1}>
          {this.props.fetching && <Loading />}
          <Paper className={classes.root} elevation={1}>
            <div className={classes.articleItem}>
              <div className={classes.img}>
                <img
                  style={{ width: "100%" }}
                  src={this.props.articleDetails.imageUrl || NatureImg}
                  alt="img"
                />
                <div className={classes.info}>
                  <h3
                    style={{
                      color: "#0288d1",
                      fontFamily: "Josefin Slab, serif",
                      fontSize: "20px"
                    }}
                  >
                    {this.props.articleDetails.title}
                  </h3>
                  <span style={{ fontFamily: "Josefin Slab, serif" }}>
                    in:
                    <span
                      style={{
                        color: categories[this.props.articleDetails.category]
                      }}
                    >
                      {" "}
                      {this.props.articleDetails.category}{" "}
                    </span>
                  </span>

                  <p
                    style={{
                      color: "#bdbdbd",
                      fontFamily: "Josefin Slab, serif"
                    }}
                  >
                    {" "}
                    Posted:{" "}
                    {moment(this.props.articleDetails.date, "X").fromNow()}
                  </p>
                </div>
              </div>
              <div className={classes.txt}>
                {this.props.articleDetails.description}
              </div>
              <div
                className={classe.ratingDisplay}
                style={{
                  marginTop: "10px",
                  fontFamily: "Pacifico , cursive",
                  fontSize: "18px",
                  marginBottom: "10px"
                }}
              >
                Rating:{this.props.articleDetails.rating}
                <div className={classe.youRate}>
                  <p> Vote :</p>
                  <div className={classe.rateIt}>
                    <button
                      className={classNames(
                        classe.triangleUp,
                        upDisable && classe.triangleUpDisable
                      )}
                      onClick={this.updateVote(1)}
                      disabled={upDisable}
                    />
                    <button
                      className={classNames(
                        classe.triangleDown,
                        downDisabled && classe.triangleDownDisabled
                      )}
                      onClick={this.updateVote(-1)}
                      disabled={downDisabled}
                    />
                  </div>
                </div>
              </div>
            </div>
            {this.props.articleDetails.comments.map((comment, id) => (
              <div key={comment.id}>
                <div>
                  {comment.userInfo.firstname + " " + comment.userInfo.lastname}
                </div>
                <div>{comment.content}</div>
              </div>
            ))}

            <form className={classes.form}>
              Write a comment :
              <TextField
                id="outlined-Comment-input"
                label="Comment"
                classes={{ root: classe.textField }}
                type="Comment"
                name="Comment"
                autoComplete="Comment"
                margin="normal"
                variant="outlined"
                multiline
                onChange={this.onCommentChange}
                value={this.state.comment}
              />
              <div className={classe.positionButton}>
                <div />

                <LoadingButton
                  classes={{
                    root: classNames(classe.button)
                  }}
                  disabled={!this.state.comment || this.props.commentFetching}
                  onClick={this.createComment}
                  title={"Comment"}
                  fetch={this.props.commentFetching}
                />
              </div>
            </form>
          </Paper>
        </div>

        <div className={classe.listeArticle2}>
          <TopRated
          onClick={this.props.onArticleClick} />
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    fetching: state.details.fetching,
    error: state.details.error,
    articleDetails: state.details.articleDetails,
    user: state.register.user,
    commentFetching: state.comment.fetching,
    commentError: state.comment.error
  };
};

const mapDisaptchToProps = dispatch => {
  return {
    articleDetailsRequest: id => dispatch(articleDetailsRequest(id)),
    updateArticle: payload => dispatch(updateArticle(payload)),
    createComment: (articleId, values) =>
      dispatch(createComment(articleId, values))
  };
};

export default connect(
  mapStateToProps,
  mapDisaptchToProps
)(withStyles(styles)(details));
