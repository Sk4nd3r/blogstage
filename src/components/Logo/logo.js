import React from "react";
import Logo from "../../assests/images/SS.png";
import classes from "./logo.module.css";

const logo = props => (
  <div className={classes.logo}>
    <img src={Logo} height="40px" alt="myLogo" />
  </div>
);

export default logo;
