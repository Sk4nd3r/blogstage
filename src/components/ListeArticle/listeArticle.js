import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Paper from "@material-ui/core/Paper";
import firebase from "firebase";
import listeArticle from "../../redux/action/listeArticleAction";
import classe from "./listeArticle.module.css";
import ArticleItems from "../articleItem/articleItem";
import articleItemTopRated from "../articleItemTopRated/articleItemTopRated";
import { listeArticleRequest } from "../../redux/action/listeArticleAction";
import TopRated from "../topRated/topRated";
import Loading from "../Loading/Loading";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    background: "#fff"
  }
});

class ListArticles extends React.Component {
  async componentDidMount() {
    this.props.listeArticleRequest();
  }
   
 
  render() {
    const { classes } = this.props;

    return (
      <div className={classe.articles}>
        {this.props.fetching && <Loading />}
        <div className={classe.listeArticle1}>
          {this.props.listArticle.length ? (
            <Paper className={classes.root} elevation={1}>
              {this.props.listArticle.map(article => (
                <ArticleItems
                  imgUrl={article.imageUrl}
                  category={article.category}
                  title={article.title}
                  description={article.description}
                  key={article.id}
                  date={article.date}
                  onClick={() => this.props.onArticleClick(article.id)}
                  
                />
              ))}
            </Paper>
          ) : null}
        </div>
        <div className={classe.listeArticle2}>
          <TopRated 
          onClick={this.props.onArticleClick}
 />
          
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    fetching: state.list.fetching,
    error: state.list.error,
    listArticle: state.list.listArticle

  };
};

const mapDisaptchToProps = dispatch => {
  return {
    listeArticleRequest: () => dispatch(listeArticleRequest()),
  };
};

export default connect(
  mapStateToProps,
  mapDisaptchToProps
)(withStyles(styles)(ListArticles));
