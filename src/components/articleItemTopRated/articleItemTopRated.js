import React from "react";
import moment from "moment";
import classes from "./articleItemTopRated.module.css";
import natureImg from "../../assests/images/nature.jpg";

import { categories } from "../../containers/PaperCategories/colorCategories/colorCategories";
const ArticleItem = props => {
  return (
    <div className={classes.articleItem}
    onClick={props.onClick} >
      <div className={classes.img}>
        <img
          src={props.imgUrl || natureImg}
          alt="nature"
          width={115}
          height={55}
        />
      </div>
      <div>
        <h3 className={classes.title}>{props.title}</h3>

        <span className={classes.post}>
          {" "}
          Posted: {moment(props.date, "X").format("DD MMMM YYYY")}
        </span>
      </div>
      <span className={classes.catego}>
        in:
        <span style={{ color: categories[props.category] }}>
          {" "}
          {props.category}{" "}
        </span>
      </span>
      <div>
        <span className={classes.catego1}>Rating:  {props.rating}</span>
      </div>
    </div>
  );
};

export default ArticleItem;
