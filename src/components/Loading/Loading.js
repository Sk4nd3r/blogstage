import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import classes from "./Loading.module.css";

const Loading = () => (
  <div className={classes.loadingContainer}>
    <div className={classes.backdrop} />
    <div className={classes["whirly-loader"]} />
  </div>
);

export default Loading;
