import React from "react";
import moment from 'moment'
import classes from "./articleItem.module.css";
import natureImg from "../../assests/images/nature.jpg";

import { categories } from "../../containers/PaperCategories/colorCategories/colorCategories";
const ArticleItemDetails = props => {
  return (
    <div className={classes.articleItem} onClick={props.onClick}>
      <div className={classes.img}>
        <img
          src={props.imgUrl || natureImg}
          alt="nature"
          width={115}
          height={55}
        />
        <div className={classes.info}>
          <h3>{props.title}</h3>
          <span>
            in:
            <span style={{ color: categories[props.category] }}>
              {" "}
              {props.category}{" "}
            </span>
          </span>

          <p> Posted:  {moment(props.date, 'X').fromNow()}</p>
        </div>
      </div>

      <div className={classes.txt}>{props.description}</div>
    </div>
  );
};

export default ArticleItemDetails;
