import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Paper from "@material-ui/core/Paper";
import firebase from "firebase";
import listeArticle from "../../redux/action/listeArticleAction";
import classe from "./topRated.module.css";
import ArticleItemsTopRated from "../articleItemTopRated/articleItemTopRated";
import { topRatedRequest } from "../../redux/action/topRatedAction";
import { withRouter } from "react-router-dom";
const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    background: "#fff"
  }
});

class topRated extends React.Component {
  async componentDidMount() {
    this.props.topRatedRequest();
  }
  render() {
    const { classes } = this.props;

    return (
      <div className={classe.topRated} 
      >
        <Paper className={classes.root}>
         <span className={classe.title} > ⭐TOP RATED⭐</span>
          {this.props.topArticle.length ? (
            <Paper elevation={1}>
              {this.props.topArticle.map(topRated => (
                <ArticleItemsTopRated
                  key={topRated.date}
                  imgUrl={topRated.imageUrl}
                  category={topRated.category}
                  title={topRated.title}
                  date={topRated.date}
                  description={topRated.description}
                  rating={topRated.rating}
                  onClick={() => this.props.onClick(topRated.id)}                />
              ))}
            </Paper>
          ) : null}
        </Paper>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    topArticleFetching: state.top.topArticleFetching,
    topArticleError: state.top.topArticleError,
    topArticle: state.top.topRated
  };
};

const mapDisaptchToProps = dispatch => {
  return {
    topRatedRequest: () => dispatch(topRatedRequest())
  };
};

export default connect(
  mapStateToProps,
  mapDisaptchToProps
)(withStyles(styles)(topRated));
