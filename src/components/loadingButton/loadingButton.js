import React, { Component } from "react";
import classes from "./loadingButton.module.css";
import classNames from "classnames";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";

class buttonCheck extends Component {

    static defaultProps = {
        title: 'Register'
    }


    state = {
        hover: false
    }

  registerHoverIn = () => {
    this.setState({ hover: true });
  };
  registerHoverOut = () => {
    this.setState({ hover: false });
  };
  render() {
      const {fetch,title, className, ...rest} = this.props
    return (
      <Button
        onMouseEnter={this.registerHoverIn}
        onMouseLeave={this.registerHoverOut}
        classes={{
          root: classNames(
            classes.loginButtonRoot,
            className,
            this.state.hover && classes.loginButtonRootHover
          )
        }}
        {...rest}
      >
        {title}
        <div className={classes.spinner}>
          {fetch && <CircularProgress size={24} color="inherit" />}

        </div>
      </Button>
    );
  }
}
export default buttonCheck;
