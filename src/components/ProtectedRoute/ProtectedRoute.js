import React from "react";
import { Redirect, Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";

const ProtectedRoute = ({ user, location, ...props }) => {
  const pathname = encodeURIComponent(location.pathname + location.search) ;
  if (!Object.keys(user).length)
    return (
      <Redirect to={{ pathname: "/login", search: "?path=" + pathname }} />
    );
  return <Route {...props} />;
};

const mapStateToProps = state => ({
  user: state.register.user
});

export default connect(mapStateToProps)(withRouter(ProtectedRoute));
