import React, { Component } from "react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import BlogBuilder from "./containers/blogBuilder";
import Dialog from "@material-ui/core/Dialog/Dialog";
import Login from "./containers/Login/Login";
import { matchPath } from "react-router-dom";
import Register from "./containers/Register/Register";
import Details from "./containers/details/details";
import categories from "./containers/Categories/categories";
import ListeArticle from "./components/ListeArticle/listeArticle";
import Profil from "./containers/Profil/Profil";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";
import Postbuilder from "./containers/postbuilder/Postbuilder";
import { startup } from "./redux/action/startupActions";
import TopRated from "./components/topRated/topRated";

class App extends Component {
  state = {
    open: false
  };

  componentDidMount() {
    this.props.startup();
  }

  loginClick = () => {
    this.props.history.push("/login");
  };

  onLogoClick = () => {
    this.props.history.push("/");
  };
  onCategoriesClick = () => {
    this.props.history.push("/categories");
  };

  onProfileClick = () => {
    this.props.history.push("/profil");
  };

  onArticleClick = id => {
    this.props.history.push("/details/" + id);
  };
  onCreatePostClick = () => {
    this.props.history.push("/create-post");
  };

  close = () => {
    this.props.history.goBack();
  };

  render() {
    if (this.props.startupState) return <div />;
    const isLogin = matchPath(this.props.location.pathname, "/login");
    return (
      <div className={"app-wrapper"}>
        <div className={"app-wrapper-opacity"}>
          <BlogBuilder
            onLogoClick={this.onLogoClick}
            onLogin={this.loginClick}
            onCategoriesClick={this.onCategoriesClick}
            onProfileClick={this.onProfileClick}

          />
          
          <Switch>
            <Route isExact path="/categories" component={categories} />
            <Route isExact path="/register" component={Register} />
            <Route isExact path="/details/:id" component={Details} />
            <ProtectedRoute isExact path="/profil" component={Profil} />

            <ProtectedRoute
              isExact
              path="/create-post"
              component={Postbuilder}
            />
            <Route
              path="/"
              render={props => {
                return (
                  <ListeArticle
                    {...props}
                    onArticleClick={this.onArticleClick}
                  />
                 
                );
              }}
            />
           {/*   <Route
              path="/"
              render={props => {
                return (
                  <TopRated
                  {...props}
                  onArticleClick={this.onArticleClick}
                />
                 
                );
              }}
            /> */}
            
          </Switch>
          <Dialog onClose={this.close} open={!!(isLogin && isLogin.isExact)}>
            <Login />
          </Dialog>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  startupState: state.startup.startup
});

const mapDisaptchToProps = dispatch => ({ startup: () => dispatch(startup()) });

export default connect(
  mapStateToProps,
  mapDisaptchToProps
)(App);
