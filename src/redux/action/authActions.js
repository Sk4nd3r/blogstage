

export const EMAIL_CHANGE = 'EMAIL_CHANGE'
export const PASSWORD_CHANGE = 'PASSWORD_CHANGE'



export const emailChange = (email) => {
    return {
        type: EMAIL_CHANGE,
        email,
    }
}   
export const passwordChange = (password) => {
    return {
        type: PASSWORD_CHANGE,
        password,

    }
}
