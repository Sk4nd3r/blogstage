import {
  createComment as createCommentRequest,
  getArticleDetails
} from "../../utils/firebaseFunctions";
import { articleDetailsSuccess } from "./articleDetailsAction";

export const CREATE_COMMENT_FETCHING = "CREATE_COMMENT_FETCHING";
export const CREATE_COMMENT_SUCCESS = "CREATE_COMMENT_SUCCESS";
export const CREATE_COMMENT_ERROR = "CREATE_COMMENT_ERROR";

export const createCommentFetching = () => {
  return { type: CREATE_COMMENT_FETCHING };
};

export const createCommentSuccess = () => {
  return { type: CREATE_COMMENT_SUCCESS };
};

export const createCommentError = error => {
  return { error, type: CREATE_COMMENT_ERROR };
};

export const createComment = (articleId, values) => {
  return async dispatch => {
    dispatch(createCommentFetching());
    try {
      await createCommentRequest(articleId, values);
      const articleDetails = await getArticleDetails(articleId);
      await dispatch(articleDetailsSuccess(articleDetails));
      dispatch(createCommentSuccess());
    } catch (e) {
      dispatch(createCommentError(e));
    }
  };
};
