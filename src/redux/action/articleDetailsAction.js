import {
  getArticleDetails,
  updateArticle as updateArticleRequest
} from "../../utils/firebaseFunctions";

export const ARTICLE_DETAILS_FETCHING = "ARTICLE_DETAILS_FETCHING";
export const ARTICLE_DETAILS_SUCCESS = "ARTICLE_DETAILS_SUCCESS";
export const ARTICLE_DETAILS_ERROR = "ARTICLE_DETAILS_ERROR";

export const articleDetailsFetching = () => {
  return { type: ARTICLE_DETAILS_FETCHING };
};

export const articleDetailsSuccess = articleDetails => {
  return { type: ARTICLE_DETAILS_SUCCESS, articleDetails };
};

export const articleDetailsError = error => {
  return { type: ARTICLE_DETAILS_ERROR, error };
};

export const articleDetailsRequest = id => {
  return async (dispatch, getState) => {
    dispatch(articleDetailsFetching());
    try {
      const articleDetails = await getArticleDetails(id);
      dispatch(articleDetailsSuccess(articleDetails));
    } catch (e) {
      dispatch(articleDetailsError(e));
    }
  };
};

export const updateArticle = payload => {
  return async (dispatch, getState) => {
    dispatch(articleDetailsFetching());
    try {
      const rating= Object.values(payload.ratingDetails).reduce(
        (sum, current) => sum + current,
        0
      )
      await updateArticleRequest({...payload,rating});
      dispatch(articleDetailsRequest(payload.id));
    } catch (e) {
      articleDetailsError(e);
    }
  };
};
