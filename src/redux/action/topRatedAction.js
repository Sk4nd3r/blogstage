import { getCollectionTopRated  } from "../../utils/firebaseFunctions";

export const TOP_RATED_FETCHING = "TOP_RATED_FETCHING";
export const TOP_RATED_SUCCESS = "TOP_RATED_SUCCESS";
export const TOP_RATED_ERROR = "TOP_RATED_ERROR";

export const topRatedFetching = () => {
  return { type: TOP_RATED_FETCHING };
};

export const topRatedSuccess = topRated => {
  return { type: TOP_RATED_SUCCESS, topRated };
};

export const topRatedError = error => {
  return { type: TOP_RATED_ERROR, error };
};

export const topRatedRequest = () => {
  return async (dispatch, getState) => {
    dispatch(topRatedFetching());
    try {
      const topRated = await getCollectionTopRated();
      dispatch(topRatedSuccess(topRated));
    } catch (e) {
      dispatch(topRatedError(e));
    }
  };
};
