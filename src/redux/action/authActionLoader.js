export const FETCH_START = "FETCH_START";
export const FETCH_END = "FETCH_END";

export const fetchStart = () => {
  return {
    type: FETCH_START
  };
};

export const fetchEnd = payload => {
  return {
    type: FETCH_END,
    user: {},
    error: ""
  };
};
