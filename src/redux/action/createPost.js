import { uploadImage, createPost } from "../../utils/firebaseFunctions";
import moment from "moment";

export const CREATE_POST_FETCHING = "CREATE_POST_FETCHING";
export const CREATE_POST_SUCCESS = "CREATE_POST_SUCCESS";
export const CREATE_POST_ERROR = "CREATE_POST_ERROR";

export const createPostFetching = () => {
  return { type: CREATE_POST_FETCHING };
};

export const createPostSuccess = () => {
  return { type: CREATE_POST_SUCCESS };
};

export const createPostError = error => {
  return { type: CREATE_POST_ERROR, error };
};

export const createPostRequest = payload => {
  return async (dispatch, getState) => {
    dispatch(createPostFetching());
    try {
      const state = getState();

      const articleParams = {
        category: payload.category,
        date: moment().unix(),
        title: payload.title,
        userId: state.register.user.uid,
        ratingDetails: {},
        rating:0
      };

      if (payload.image) {
        const imageUrl = await uploadImage(payload.image);
        articleParams.imageUrl = imageUrl;
      }
      if (payload.description) {
        articleParams.description = payload.description;
      }
      await createPost(articleParams);
      dispatch(createPostSuccess());
    } catch (e) {
      dispatch(createPostError(e.message));
    }
  };
};
