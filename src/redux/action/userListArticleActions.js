import { userListArticle } from "../../utils/firebaseFunctions";

export const USER_LIST_ARTICLE_FETCHING = "USER_LIST_ARTICLE_FETCHING";
export const USER_LIST_ARTICLE_SUCCESS = "USER_LIST_ARTICLE_SUCCESS";
export const USER_LIST_ARTICLE_ERROR = "USER_LIST_ARTICLE_ERROR";

export function userListArticleFetching() {
  return { type: USER_LIST_ARTICLE_FETCHING };
}

export function userListArticleSuccess(articles) {
  return { type: USER_LIST_ARTICLE_SUCCESS, articles };
}

export function userListArticleError(error) {
  return { type: USER_LIST_ARTICLE_SUCCESS, error };
}

export function userListArticleRequest(userId) {
  return async dispatch => {
    try {
      dispatch(userListArticleFetching());
      const articles = await userListArticle(userId);
      dispatch(userListArticleSuccess(articles));
    } catch (e) {
      dispatch(userListArticleError(e));
    }
  };
}
