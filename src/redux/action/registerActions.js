import firebase from "firebase";

export const REGISTER_FETCHING = "REGISTER_FETCHING";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_ERROR = "REGISTER_ERROR";

export const registerFetching = () => {
  return { type: REGISTER_FETCHING };
};

export const registerSuccess = user => {
  return { type: REGISTER_SUCCESS, user };
};

export const registerError = error => {
  return { type: REGISTER_ERROR, error };
};

export const registerRequest = payload => {
  return async (dispatch, getState) => {
    dispatch(registerFetching());
    try {
      const logged = await firebase
        .auth()
        .createUserWithEmailAndPassword(payload.email, payload.password);

      
    
      await logged.user.updateProfile({
        displayName: `${payload.firstname} ${payload.lastname}`
      });

    
      await firebase
      .firestore()
      .collection("user")
      .add({
        firstname: payload.firstname,
        lastname: payload.lastname,
        id: logged.user.uid
      });

      dispatch(registerSuccess(logged.user));
    } catch (e) {
      dispatch(registerError(e.message));
    }
  };
};
