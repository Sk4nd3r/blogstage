import { getCollectionList } from "../../utils/firebaseFunctions";

export const LISTE_ARTICLE_FETCHING = "LISTE_ARTICLE_FETCHING";
export const LISTE_ARTICLE_SUCCESS = "LISTE_ARTICLE_SUCCESS";
export const LISTE_ARTICLE_ERROR = "LISTE_ARTICLE_ERROR";

export const listeArticleFetching = () => {
  return { type: LISTE_ARTICLE_FETCHING };
};

export const listeArticleSuccess = listeArticle => {
  return { type: LISTE_ARTICLE_SUCCESS, listeArticle };
};

export const listeArticleError = error => {
  return { type: LISTE_ARTICLE_ERROR, error };
};

export const listeArticleRequest = () => {
  return async (dispatch, getState) => {
    dispatch(listeArticleFetching());
    try {
      const listArtsicles = await getCollectionList();
      dispatch(listeArticleSuccess(listArtsicles));
    } catch (e) {
      dispatch(listeArticleError(e));
    }
  };
};
