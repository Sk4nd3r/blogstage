import firebase from "firebase";
import { registerSuccess } from "./registerActions";

export const STARTUP_END = "STARTUP_END";

export const startupEnd = () => {
  return { type: STARTUP_END };
};

export const startup = () => {
  return async dispatch => {
    firebase.auth().onAuthStateChanged(user => {
      dispatch(registerSuccess(user || {}));
      dispatch(startupEnd());
    });
  };
};
