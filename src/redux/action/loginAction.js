import firebase from "firebase";
import { registerSuccess } from "./registerActions";

export const LOGIN_FETCHING = "LOGIN_FETCHING";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";

export const loginFetching = () => {
  return { type: LOGIN_FETCHING };
};

export const loginSuccess = () => {
  return { type: LOGIN_SUCCESS };
};

export const loginError = error => {
  return { type: LOGIN_ERROR, error };
};

export const loginRequest = payload => {
  return async (dispatch, getState) => {
    dispatch(loginFetching());
    try {
      const logged = await firebase
        .auth()
        .signInWithEmailAndPassword(payload.email, payload.password);
      await dispatch(registerSuccess(logged.user));
      dispatch(loginSuccess());
    } catch (e) {
      dispatch(loginError(e.message));
    }
  };
};

export const logout = () => {
  return async (dispatch, getState) => {
    try {
      await firebase.auth().signOut();
      dispatch(registerSuccess({}));
    } catch (e) {}
  };
};
