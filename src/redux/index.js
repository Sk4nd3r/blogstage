import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";

import authReducer from "./reducers/authReducer";
import loaderReducer from "./reducers/authReducerLoader";
import registerReducer from "./reducers/registerReducer";
import listeArticle from "./reducers/listeArticleReducer";
import loginReducer from "./reducers/loginReducer";
import topRatedReducer from "./reducers/topRatedReducer";
import articleDetails from "./reducers/articleDetailsReducer";
import startup from "./reducers/startup";
import comment from "./reducers/commentReducer";
import userListArticle from "./reducers/userListArticleReducer";

const middlewares = [thunk, logger];

const reducer = combineReducers({
  auth: authReducer,
  register: registerReducer,
  loader: loaderReducer,
  list: listeArticle,
  login: loginReducer,
  top: topRatedReducer,
  details: articleDetails,
  startup,
  comment,
  userListArticle
});

export default createStore(reducer, applyMiddleware(...middlewares));
