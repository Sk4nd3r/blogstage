import { STARTUP_END } from "../action/startupActions";

const intialState = {
  startup: true
};

export default (state = intialState, action) => {
  if (action.type === STARTUP_END) return { startup: false };
  return state;
};
