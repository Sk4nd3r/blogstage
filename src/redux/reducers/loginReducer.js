import {
  LOGIN_FETCHING,
  LOGIN_SUCCESS,
  LOGIN_ERROR
} from "../action/loginAction";

const intialState = {
  fetching: false,
  error: ""
};

export default (state = intialState, action) => {
  switch (action.type) {
    case LOGIN_FETCHING: {
      return { ...state, fetching: true, error: "" };
    }
    case LOGIN_SUCCESS: {
      return { ...state, fetching: false };
    }

    case LOGIN_ERROR: {
      return { ...state, error: action.error, fetching: false };
    }

    default: {
      return state;
    }
  }
};
