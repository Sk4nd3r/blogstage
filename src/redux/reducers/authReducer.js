import { EMAIL_CHANGE, PASSWORD_CHANGE } from "../action/authActions";

const intialState = {
  email: "",
  password: ""
};


export default (state = intialState, action) => {
  console.log("action auth", action);

  switch (action.type) {
    case EMAIL_CHANGE: {
      return { ...state, email: action.email };
    }
    case PASSWORD_CHANGE: {
      return { ...state, password: action.password };
    }

    default: {
      return state;
    }
  }
};
