import {
  CREATE_POST_FETCHING,
  CREATE_POST_SUCCESS,
  CREATE_POST_ERROR
} from "../action/createPost";

const intialState = {
  fetching: false,
  error: ""
};

export default (state = intialState, action) => {
  switch (action.type) {
    case CREATE_POST_FETCHING: {
      return { ...state, fetching: true, error: "" };
    }
    case CREATE_POST_SUCCESS: {
      return { ...state, fetching: false };
    }

    case CREATE_POST_ERROR: {
      return { ...state, error: action.error, fetching: false };
    }

    default: {
      return state;
    }
  }
};
