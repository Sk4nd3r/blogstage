import { FETCH_START, FETCH_END } from "../../redux/action/authActionLoader";

const intialState = {
  fetch: false,
  user:{},
  error:''
};

export default (state = intialState, action) => {
  switch (action.type) {
    case FETCH_START: {
      return { ...state, fetch: true };
    }
    case FETCH_END: {
      return { ...state, fetch: false };
    }
    default: {
      return state;
    }
  }
};
