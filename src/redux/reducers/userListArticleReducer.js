import {
  USER_LIST_ARTICLE_ERROR,
  USER_LIST_ARTICLE_FETCHING,
  USER_LIST_ARTICLE_SUCCESS
} from "../action/userListArticleActions";

const intialState = {
  fetching: false,
  articles: [],
  error: ""
};

export default function(state = intialState, action) {
  switch (action.type) {
    case USER_LIST_ARTICLE_FETCHING:
      return { ...state, fetching: true, error: "" };
    case USER_LIST_ARTICLE_SUCCESS:
      return { ...state, fetching: false, articles: action.articles };

    case USER_LIST_ARTICLE_ERROR:
      return { ...state, fetching: false, error: action.error };
    default:
      return state;
  }
}
