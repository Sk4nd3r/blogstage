import {
  REGISTER_FETCHING,
  REGISTER_SUCCESS,
  REGISTER_ERROR
} from "../action/registerActions";

const intialState = {
  fetching: false,
  error: "",
  user: {}
};

export default (state = intialState, action) => {
  switch (action.type) {
    case REGISTER_FETCHING: {
      return { ...state, fetching: true, error: "" };
    }
    case REGISTER_SUCCESS: {
      return { ...state, fetching: false, user: action.user };
    }

    case REGISTER_ERROR: {
      return { ...state, error: action.error, fetching: false };
    }
    

    default: {
      return state;
    }
  }
};
