import {
  CREATE_COMMENT_FETCHING,
  CREATE_COMMENT_SUCCESS,
  CREATE_COMMENT_ERROR
} from "../action/commentActions";

const intialState = {
  fetching: false,
  error: ""
};

export default (state = intialState, action) => {
  switch (action.type) {
    case CREATE_COMMENT_FETCHING:
      return { ...state, fetching: true, error: "" };
    case CREATE_COMMENT_SUCCESS:
      return { ...state, fetching: false };
    case CREATE_COMMENT_ERROR:
      return { ...state, fetching: false, error: action.error };
    default:
      return state;
  }
};
