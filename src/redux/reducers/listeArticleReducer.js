import { LISTE_ARTICLE_FETCHING, LISTE_ARTICLE_SUCCESS, LISTE_ARTICLE_ERROR } from"../action/listeArticleAction";

const intialState = {
    fetching: false,
    error: "",
    listArticle:[]
  };

  export default (state = intialState, action) => {
    switch (action.type) {
      case LISTE_ARTICLE_FETCHING: {
        return { ...state, fetching: true, error: "" };
      }
      case LISTE_ARTICLE_SUCCESS: {
        return { ...state, fetching: false, listArticle: action.listeArticle };
      }
  
      case LISTE_ARTICLE_ERROR: {
        return { ...state, error: action.error, fetching: false };
      }
     
  
      default: {
        return state;
      }
    }
  };