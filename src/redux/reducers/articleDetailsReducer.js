import {
  ARTICLE_DETAILS_FETCHING,
  ARTICLE_DETAILS_SUCCESS,
  ARTICLE_DETAILS_ERROR
} from "../action/articleDetailsAction";

const intialState = {
  fetching: false,
  error: "",
  articleDetails: { userInfo: {}, ratingDetails: {}, comments: [] }
};

export default (state = intialState, action) => {
  switch (action.type) {
    case ARTICLE_DETAILS_FETCHING: {
      return { ...state, fetching: true, error: "" };
    }

    case ARTICLE_DETAILS_SUCCESS: {
      return {
        ...state,
        fetching: false,
        articleDetails: action.articleDetails
      };
    }

    case ARTICLE_DETAILS_ERROR: {
      return { ...state, error: action.error, fetching: false };
    }

    default: {
      return state;
    }
  }
};
