import { TOP_RATED_FETCHING, TOP_RATED_SUCCESS, TOP_RATED_ERROR } from"../action/topRatedAction";

const intialState = {
    fetching: false,
    error: "",
    topRated:[]
  };

  export default (state = intialState, action) => {
    switch (action.type) {
      case TOP_RATED_FETCHING: {
        return { ...state, fetching: true, error: "" };
      }
      case TOP_RATED_SUCCESS: {
        return { ...state, fetching: false, topRated: action.topRated };
      }
  
      case TOP_RATED_ERROR: {
        return { ...state, error: action.error, fetching: false };
      }
     
  
      default: {
        return state;
      }
    }
  };