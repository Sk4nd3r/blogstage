import axios from 'axios';

const instance = axios.create({
    baseURL:"https://sea-stalk.firebaseio.com/"
});

export default instance;